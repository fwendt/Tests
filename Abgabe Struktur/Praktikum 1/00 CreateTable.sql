 
/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     28.04.2017 07:47:31                          */
/*==============================================================*/


drop index ZIEL_FK;

drop index START_FK;

drop index FLIEGT_FK;

drop index FLUG_PK;

drop table FLUG;

drop index FLUGHAFEN_PK;

drop table FLUGHAFEN;

drop index FLUGZEUG_PK;

drop table FLUGZEUG cascade;

drop index WARTUNG_FK;

drop index PROTOKOLL_PK;

drop table PROTOKOLL;

/*==============================================================*/
/* Table: FLUG                                                  */
/*==============================================================*/
create table FLUG (
   FLUGNUMMER           VARCHAR(8)           not null,
   START                VARCHAR(6)           not null,
   ZIEL                 VARCHAR(6)           not null,
   KENNZEICHEN          VARCHAR(8)           not null,
   constraint PK_FLUG primary key (FLUGNUMMER)
);

/*==============================================================*/
/* Index: FLUG_PK                                               */
/*==============================================================*/
create unique index FLUG_PK on FLUG (
FLUGNUMMER
);

/*==============================================================*/
/* Index: FLIEGT_FK                                             */
/*==============================================================*/
create  index FLIEGT_FK on FLUG (
KENNZEICHEN
);

/*==============================================================*/
/* Index: START_FK                                              */
/*==============================================================*/
create  index START_FK on FLUG (
START
);

/*==============================================================*/
/* Index: ZIEL_FK                                               */
/*==============================================================*/
create  index ZIEL_FK on FLUG (
ZIEL
);

/*==============================================================*/
/* Table: FLUGHAFEN                                             */
/*==============================================================*/
create table FLUGHAFEN (
   IATA                 VARCHAR(6)           not null,
   NAME                 VARCHAR(80)          null,
   LATITUDE             FLOAT8               null,
   LONGITUDE            FLOAT8               null,
   constraint PK_FLUGHAFEN primary key (IATA)
);

/*==============================================================*/
/* Index: FLUGHAFEN_PK                                          */
/*==============================================================*/
create unique index FLUGHAFEN_PK on FLUGHAFEN (
IATA
);

/*==============================================================*/
/* Table: FLUGZEUG                                              */
/*==============================================================*/
create table FLUGZEUG (
   KENNZEICHEN          VARCHAR(8)           not null,
   TYP                  VARCHAR(20)          null,
   SITZPLATZE           INT4                 null,
   constraint PK_FLUGZEUG primary key (KENNZEICHEN)
);

/*==============================================================*/
/* Index: FLUGZEUG_PK                                           */
/*==============================================================*/
create unique index FLUGZEUG_PK on FLUGZEUG (
KENNZEICHEN
);

/*==============================================================*/
/* Table: PROTOKOLL                                             */
/*==============================================================*/
create table PROTOKOLL (
   KENNZEICHEN          VARCHAR(8)           not null,
   DATUM                DATE                 not null,
   FREIGABE             BOOL                 null,
   constraint PK_PROTOKOLL primary key (KENNZEICHEN, DATUM)
);

/*==============================================================*/
/* Index: PROTOKOLL_PK                                          */
/*==============================================================*/
create unique index PROTOKOLL_PK on PROTOKOLL (
KENNZEICHEN,
DATUM
);

/*==============================================================*/
/* Index: WARTUNG_FK                                            */
/*==============================================================*/
create  index WARTUNG_FK on PROTOKOLL (
KENNZEICHEN
);

alter table FLUG
   add constraint FK_FLUG_FLIEGT_FLUGZEUG foreign key (KENNZEICHEN)
      references FLUGZEUG (KENNZEICHEN)
      on delete restrict on update restrict;

alter table FLUG
   add constraint FK_FLUG_START_FLUGHAFE foreign key (START)
      references FLUGHAFEN (IATA)
      on delete restrict on update restrict;

alter table FLUG
   add constraint FK_FLUG_ZIEL_FLUGHAFE foreign key (ZIEL)
      references FLUGHAFEN (IATA)
      on delete restrict on update restrict;

alter table PROTOKOLL
   add constraint FK_PROTOKOL_WARTUNG_FLUGZEUG foreign key (KENNZEICHEN)
      references FLUGZEUG (KENNZEICHEN)
      on delete restrict on update restrict;

