delete from flug;
delete from protokoll;
delete from flugzeug;
delete from flughafen;



--Daten für die Flughäfen nach folgendem Muster:
--NAME (IATA)
--BREITENGRAD, LÄNGENGRAD
--delete from flughafen;
insert into flughafen(name, iata, latitude, longitude)
values ('Hartsfield-Jackson International Airport Atlanta','ATL',33.636667,-84.428056);
insert into flughafen(name, iata, latitude, longitude)
values ('Beijing Capital International Airport','PEK',40.0725,116.5975);
insert into flughafen(name, iata, latitude, longitude)
values ('Dubai International Airport','DXB',25.252778,55.364444);
insert into flughafen(name, iata, latitude, longitude)
values ('Chicago O Hare International Airport','ORD',41.978611,-87.904722);
insert into flughafen(name, iata, latitude, longitude)
values ('Tokyo International Airport','HND',35.553333,139.781111);
insert into flughafen(name, iata, latitude, longitude)
values ('Heathrow Airport','LHR',51.4775,-0.461389);
insert into flughafen(name, iata, latitude, longitude)
values ('Los Angeles International Airport','LAX',33.9425,-118.408056);
insert into flughafen(name, iata, latitude, longitude)
values ('Hong Kong International Airport','HKG',22.308889,113.914444);
insert into flughafen(name, iata, latitude, longitude)
values ('Paris Charles de Gaulle Airport','CDG',49.009722,2.547778);
insert into flughafen(name, iata, latitude, longitude)
values ('Dallas International Airport','DFW',32.896944,-97.038056);
insert into flughafen(name, iata, latitude, longitude)
values ('Istanbul Atatürk Airport','IST',40.976111,28.814167);
insert into flughafen(name, iata, latitude, longitude)
values ('Frankfurt Airport','FRA',50.033333,8.570556);

--Daten für Flugzeuge nach folgendem Muster:
--TYP, KENNZEICHEN, SITZPLÄTZE
--delete from flugzeug;
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A330-300','D-ABBB',236);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A330-300','D-ABBC',240);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A330-300','D-ABBD',221);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A330-300','D-ABBE',231);
--Airbus A330-300, D-ABBB, 236
--Airbus A330-300, D-ABBC, 240
--Airbus A330-300, D-ABBD, 221
--Airbus A330-300, D-ABBE, 231

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A340-300','D-ABBF',280);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A340-300','D-ABBG',271);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A340-300','D-ABBH',243);
--Airbus A340-300, D-ABBF, 280
--Airbus A340-300, D-ABBG, 271
--Airbus A340-300, D-ABBH, 243

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A340-600','D-ABBI',266);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A340-600','D-ABBK',293);
--Airbus A340-600, D-ABBI, 266
--Airbus A340-600, D-ABBK, 293

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A380-800','D-ABBL',509);
--Airbus A380-800, D-ABBL, 509

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A350-900','D-ABBM',293);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A350-900','D-ABBO',297);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A350-900','D-ABBP',288);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A350-900','D-ABBQ',288);
--Airbus A350-900, D-ABBM, 293
--Airbus A350-900, D-ABBO, 297
--Airbus A350-900, D-ABBP, 288
--Airbus A350-900, D-ABBQ, 288

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A320-200','D-ABBR',168);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A320-200','D-ABBS',168);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A320-200','D-ABBT',170);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Airbus A320-200','D-ABBU',142);
--Airbus A320-200, D-ABBR, 168
--Airbus A320-200, D-ABBS, 168
--Airbus A320-200, D-ABBT, 170
--Airbus A320-200, D-ABBU, 142

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Boeing B747-8','D-ABBW',364);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Boeing B747-8','D-ABBX',364);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Boeing B747-8','D-ABBY',364);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Boeing B747-8','D-ABBZ',364);
--Boeing B747-8, D-ABBW, 364
--Boeing B747-8, D-ABBX, 364
--Boeing B747-8, D-ABBY, 364
--Boeing B747-8, D-ABBZ, 364

insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Boeing B737-700','D-ABCA',86);
insert into flugzeug(typ,kennzeichen,sitzplatze)
values('Boeing B737-700','D-ABCC',88);
--Boeing B737-700,D-ABCA, 86
--Boeing B737-700,D-ABCC, 88

--Daten für Flüge nach folgendem Muster:
--FLUGNUMMER, FLUGZEUG, START, ZIEL
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-100','D-ABBB','FRA','ATL');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-101','D-ABBF','FRA','PEK');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-102','D-ABBI','FRA','DXB');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-103','D-ABBI','FRA','ORD');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-104','D-ABBL','FRA','HND');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-105','D-ABBW','FRA','LHR');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-106','D-ABBX','FRA','LAX');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-107','D-ABBY','FRA','HKG');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-108','D-ABCC','FRA','CDG');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-109','D-ABCA','FRA','DFW');
insert into flug(flugnummer, kennzeichen, start, ziel)
values('LH-110','D-ABBQ','FRA','IST');
--LH-100, D-ABBB, FRA, ATL
--LH-101, D-ABBF, FRA, PEK
--LH-102, D-ABBI, FRA, DXB
--LH-103, D-ABBI, FRA, ORD
--LH-104, D-ABBL, FRA, HND
--LH-105, D-ABBW, FRA, LHR
--LH-106, D-ABBX, FRA, LAX
--LH-107, D-ABBY, FRA, HKG
--LH-108, D-ABCC, FRA, CDG
--LH-109, D-ABCA, FRA, DFW
--LH-110, D-ABBQ, FRA, IST
