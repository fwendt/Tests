package FirstSteps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import AssistingClasses.TestAssistant;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author frederik
 */
public class FirstStepsTest {

    private static String databaseName = "testdb";
    private final static String DB_URL = "jdbc:postgresql://db-lab-postgres:5432/";
    
    private static Connection connection;
    private static Statement statement;

    @BeforeClass
    public static void setUpClass() {
        try {
            databaseName = TestAssistant.readFileToString("/var/jenkins_home/workspace/GradleBasedTest/DatabaseName.txt");
            TestAssistant.createTestDB(databaseName);
            System.out.println("DatabaseName: " + databaseName);

            connection = DriverManager.getConnection(DB_URL + databaseName, "dbadmin", "dbadminpassword");
            statement = connection.createStatement();
        } catch (SQLException | IOException e) {
            System.err.println(e.toString());
            fail("hier2 \n " + e.toString());
        }
    }

    @AfterClass
    public static void tearDownClass() {
//        try {
//            connection.close();
//            TestAssistant.deleteTestDB(databaseName);
//        } catch (SQLException e) {
//            System.err.println(e.toString());
//            fail();
//        }

    }

    @Test
    public void test() {
        createDBScheme();
        insertTestData();

        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM Persons;");
            while (rs.next()) {
                System.out.println(rs.getInt(1));
                System.out.println(rs.getString(2));
                System.out.println(rs.getString(3));
                System.out.println(rs.getString(4));
                System.out.println(rs.getString(5));

                assertEquals(0, rs.getInt(1));
                assertEquals("Ruehl", rs.getString(2));
                assertEquals("Stefan", rs.getString(3));
                assertEquals("idk", rs.getString(4));
                assertEquals("idk", rs.getString(5));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FirstStepsTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("test Fehler");
        }
    }

    private void createDBScheme() {
        try {
            statement.execute("CREATE TABLE Persons (\n"
                    + "    PersonID int,\n"
                    + "    LastName varchar(255),\n"
                    + "    FirstName varchar(255),\n"
                    + "    Address varchar(255),\n"
                    + "    City varchar(255)\n"
                    + ");");
        } catch (SQLException ex) {
            Logger.getLogger(FirstStepsTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("createDBScheme Fehler");
        }
    }

    private void insertTestData() {
        try {
            statement.execute("insert into Persons (PersonID, LastName, FirstName, Address, City) Values (0, 'Ruehl', 'Stefan', 'idk', 'idk');");
        } catch (SQLException ex) {
            Logger.getLogger(FirstStepsTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("insertTestData Fehler");
        }
    }
}
