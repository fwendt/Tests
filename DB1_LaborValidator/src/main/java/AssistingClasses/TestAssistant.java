package AssistingClasses;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author frederik
 */
public class TestAssistant {

    private final static String DB_URL = "jdbc:postgresql://db-lab-postgres:5432/postgres";

    public static List<String> readWholeFile(String filename) throws FileNotFoundException, IOException {
        File inputFile = new File(filename);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));
        
        List<String> returnValue = new ArrayList<>();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            returnValue.add(line);
        }
        return returnValue;
    }

    public static String readFileToString(String filename) throws FileNotFoundException, IOException {
        File inputFile = new File(filename);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));

        String returnValue = "";
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            returnValue = returnValue + line;
        }
        return returnValue;
    }

    public static void createTestDB(String databaseName) throws SQLException {
        Connection localConnection = DriverManager.getConnection(DB_URL, "dbadmin", "dbadminpassword");
        Statement localStatement = localConnection.createStatement();
        localStatement.execute("CREATE DATABASE " + databaseName);
        localConnection.close();

    }

    public static void deleteTestDB(String databaseName) throws SQLException {
        Connection localConnection = DriverManager.getConnection(DB_URL, "dbadmin", "dbadminpassword");
        Statement localStatement = localConnection.createStatement();
        localStatement.execute("DROP DATABASE " + databaseName);
        localConnection.close();
    }
}
